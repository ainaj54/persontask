package scania.aina.PersonClass;

import java.util.Scanner;

public class PersonClass {

    //PersonClass fields as defined by assignment.
    private String personName;
    private int personHeightCm;
    private String personGender;
    private String personHometown;

    //Default constructor
    public PersonClass() {

    }
    //Overload constructor
    public PersonClass(String inputName, int inputHeight, String inputGender, String inputHometown ){
        this.personName = inputName;
        this.personHeightCm = inputHeight;
        this.personGender = inputGender;
        this.personHometown = inputHometown;

    }

    //Funny introduction as requested by assignment.
    public void introducePerson(){

        int loopCounter = 1;
        while(true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Knock knock");
            String inputString = scanner.nextLine();

            if(inputString.equals("Who's there?")){
                System.out.println("I don't know, I never really got knock knock jokes.");
                System.out.println("Anyway, my name is " + this.personName + " and I'm from " + this.personHometown);
                break;
            }else if(loopCounter == 1 && !inputString.equals("Who's there?")){
                System.out.println("This answer is incorrect. Have you never heard a knock knock joke before?");
            }
            else if(loopCounter == 2 && !inputString.equals("Who's there?")){
                System.out.println("This answer is also incorrect. I'm losing my patience");
            }else if(loopCounter >= 3 && !inputString.equals("Who's there?")){
                System.out.println("WRONG!!!! ");
                System.out.println("I'm not letting you out of this loop until you answer correctly.");
            }
            ++loopCounter;
        }
    }

    //overload introduction-method. Input personclass object.
    public void introducePerson(PersonClass otherPerson){
        if(otherPerson.personHometown == this.personHometown){
            System.out.println("Don't I know you? You're from " + this.personHometown + " too? Do you know the Jorgensens?");
        }else{
            introducePerson();
        }
    }

    //method to return height in inches
    public double heightCmToInches(){
        return this.personHeightCm * 0.3937;
    }

}



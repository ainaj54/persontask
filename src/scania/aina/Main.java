package scania.aina;

import scania.aina.PersonClass.PersonClass;

public class Main {

    public static void main(String[] args) {
        PersonClass aina = new PersonClass("Aina", 170, "Female", "Gothenburg");
        PersonClass jonas = new PersonClass("Jonas", 180, "Male", "Gothenburg");
        aina.introducePerson(jonas);

        aina.introducePerson();


    }
}
